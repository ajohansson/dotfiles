# Dotfiles

Configuration files for my personal development environment.

Relies on [GNU Stow][gnu-stow] for managing symlinks to config files in this repository.

[gnu-stow]: https://www.gnu.org/software/stow/


## Getting started

### Requirements

The [installation script](./install) relies on the following tools:

- [GNU Stow][gnu-stow]
- [FZF][fzf-repo]

[fzf-repo]: https://github.com/junegunn/fzf


### Install dotfiles

The installation script allows the user to interactively select what packages to install configuraiton files for:
```bash
./install
```
When selecting packages, the options to choose from are given by the directories in `configs/`.

Selection is made using [fzf][fzf-repo] to enable fuzzy finding. Use `<CTRL-a>` to toggle selection of all packages, and `<ESC>` to abort.

### Remove dotfiles

To remove configuration files for a package:
```bash
./install --delete
```
or to simply update configuration for a package if old files have been deleted or moved:
```bash
./install --restow
```

### Add new dotfiles

Configuration files for new packages can be added by:

1. Creating a new directory in `configs/` for the package with expected file structure:
   ```bash
   mkdir -p configs/<my-package>/.config/package/
   ```
2. Create configuration files:
   ```bash
   touch configs/<my-package>/.config/package/config
   ```
3. Stow new package configuration with the installation script
   ```bash
   ./install
   ```
   and select `<my-package>`.

### Adopt existing dotfiles

Create file structure to mimic existing configuration for package:
```bash
mkdir -p <my-package>/.config/package/
```
Adopt existing configuration files:
```bash
./install --adopt
```
*NOTE:* This will move the real configuration files into this repository and replace
them with a symbolic link.


## Backup installed packages

Installed packages for different tools can be updated with:
```bash
./backup-packages
```

### Pacman
List explicilty installed packages
```bash
pacman -Qeq
```

### Rustup
List installed Rust components
```bash
rustup component list --installed
```

### Go
List paths of installed Go binaries
```bash
find $GOBIN -maxdepth 1 -type f -executable | xargs -L1 go version -m | awk '/path/ {print $2}'
```

### Fisher
Fish plugins managed by [fisher][fisher] are already traked in `fish/`, but can be listed with
```bash
fisher list
```

[fisher]: https://github.com/jorgebucaran/fisher 

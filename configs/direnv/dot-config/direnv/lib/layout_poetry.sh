layout_poetry() {
    if ! has poetry
    then
        echo "error: 'poetry' not available" >&2
        return 1
    fi

    PYPROJECT_TOML="${PYPROJECT_TOML:-pyproject.toml}"
    if [[ ! -f $PYPROJECT_TOML ]]
    then
        log_status "No $PYPROJECT_TOML found. Running \`poetry init\`..."
        poetry init
    fi

    if [[ -d ".venv" ]]
    then
        VIRTUAL_ENV="$(pwd)/.venv"
    else
        VIRTUAL_ENV="$(poetry env info --path 2>/dev/null ; true)"
    fi

    if [[ -z $VIRTUAL_ENV || ! -d $VIRTUAL_ENV ]]
    then
        log_status "No virtual environment exists. Running \`poetry install\`..."
        poetry install
        VIRTUAL_ENV="$(poetry env info --path)"
    fi

    PATH_add "$VIRTUAL_ENV/bin"
    export POETRY_ACTIVE=1
    export VIRTUAL_ENV
}

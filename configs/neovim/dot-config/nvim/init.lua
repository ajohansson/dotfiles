-- User config
require("user.options")
require("user.keybinds")
require("user.abbrev")
require("user.autocmds")
require("user.filetypes")
require("user.lsp")
require("user.statusline")
require("user.plugins")

local functions = require("user.functions")
vim.api.nvim_create_user_command("Make", functions.async_make, { nargs = '*' })
vim.ui.select = functions.fzf_ui_select

vim.cmd.cnoreabbrev({"W", "w"})
vim.cmd.cnoreabbrev({"Wq", "wq"})

vim.cmd.cnoreabbrev({"H", "vertical botright help"})

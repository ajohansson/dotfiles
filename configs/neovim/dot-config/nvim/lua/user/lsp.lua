local lsp_setup = vim.api.nvim_create_augroup('LspSetup', { clear = true })

vim.api.nvim_create_autocmd("LspAttach", {
    group = lsp_setup,
    desc = "Setup key bindings for LSP client in buffer",
    callback = function(args)
        local client = vim.lsp.get_client_by_id(args.data.client_id)
        local capabilities = client.server_capabilities

        local bind = function(mode, key, func)
            vim.keymap.set(mode, key, func, {
                buffer = args.buf,
                remap = false,
                silent = true,
            })
        end

        if capabilities.hoverProvider then
            bind('n', 'K', vim.lsp.buf.hover)
        end
        if capabilities.hoverProvider then
            bind('n', 'gK', vim.lsp.buf.signature_help)
        end
        if capabilities.definitionProvider then
            bind('n', 'gd', vim.lsp.buf.definition)
        end
        if capabilities.typeDefinitionProvider then
            bind('n', 'gD', vim.lsp.buf.type_definition)
        end
        if capabilities.implementationProvider then
            bind('n', 'gI', vim.lsp.buf.implementation)
        end
        if capabilities.referencesProvider then
            bind('n', 'gR', vim.lsp.buf.references)
        end
        if capabilities.documentSymbolProvider then
            bind('n', 'gs', vim.lsp.buf.document_symbol)
        end
        if capabilities.workspaceSymbolProvider then
            bind('n', 'gS', vim.lsp.buf.workspace_symbol)
        end
        if capabilities.documentFormattingProvider then
            bind({ 'n', 'v' }, 'gq', vim.lsp.buf.format)
        end
        if capabilities.completionProvider then
            bind('i', '.', '.<C-x><C-o>')
        end
        if capabilities.codeActionProvider then
            bind({ 'n', 'v' }, '<Leader>a', function()
                vim.lsp.buf.code_action({ apply = false })
            end)
        end
        if capabilities.renameProvider then
            bind('n', '<Leader>r', vim.lsp.buf.rename)
        end
        if capabilities.documentHighlightProvider then
            vim.cmd.highlight("link LspReferenceText DiffChange")
            vim.cmd.highlight("link LspReferenceRead Search")
            vim.cmd.highlight("link LspReferenceWrite IncSearch")

            bind('n', '<Leader>h', vim.lsp.buf.document_highlight)
            bind('n', '<Leader>H', vim.lsp.buf.clear_references)
        end

        vim.api.nvim_buf_create_user_command(
            args.buf,
            "LspStatus",
            function()
                local status = vim.lsp.buf.server_ready() and "ready" or "not ready"
                print("Server is " .. status)
            end,
            { desc = "Check LSP server status" }
        )

        if capabilities.documentFormattingProvider then
            vim.api.nvim_create_autocmd('BufWritePre', {
                desc = "Format file on save using LSP server",
                buffer = args.buf,
                callback = function()
                    vim.lsp.buf.format()
                end,
                group = lsp_setup,
            })
        end
    end,
})

-- Custom LSP handlers
vim.lsp.handlers["textDocument/documentSymbol"] = vim.lsp.with(
    vim.lsp.handlers["textDocument/documentSymbol"], {
        loclist = true,
    }
)

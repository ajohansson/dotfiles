local function bind(mode, keys, action)
    local opts = {
        remap = false,
        silent = true,
    }
    vim.keymap.set(mode, keys, action, opts)
end

vim.g.mapleader = " "

bind('i', 'jj', '<ESC>')
bind('t', '<ESC>', '<C-\\><C-n>')
bind('n', '<Leader>q', '<cmd>helpclose | cclose | lclose | pclose<CR>')
bind('n', '<Leader>g', '<cmd>Git<CR>')

bind('n', '<C-u>', '<C-u>zz')
bind('n', '<C-d>', '<C-d>zz')

bind('n', 'gd', '<C-]>')
bind('n', 'gD', '<C-]>')

bind({ 'n', 'v' }, '<Leader>y', '"+y')

bind('v', '//', 'y/<C-R>"<CR>')

-- FZF bindings
bind('n', '<Leader>fb', '<cmd>Buffers<CR>')
bind('n', '<Leader>ff', '<cmd>Files<CR>')
bind('n', '<Leader>fo', '<cmd>History<CR>')
bind('n', '<Leader>fh', '<cmd>Helptags<CR>')
bind('n', '<Leader>fl', '<cmd>BLines<CR>')
bind('n', '<Leader>fL', '<cmd>Lines<CR>')
bind('n', '<Leader>fw', '<cmd>Windows<CR>')
bind('n', '<Leader>w', '<cmd>Windows<CR>')

-- Triggering make or other build/test tools
bind('n', '<Leader>MM', '<cmd>make<CR>')
bind('n', '<Leader>MT', '<cmd>make test<CR>')
bind('n', '<Leader>mm', '<cmd>Make<CR>')
bind('n', '<Leader>mt', '<cmd>Make test<CR>')

-- Jumping to errors, warnings and diagnostics
bind('n', '<C-n>', '<cmd>cnext<CR>')
bind('n', '<C-p>', '<cmd>cprev<CR>')
bind('n', '<Leader>n', '<cmd>lnext<CR>')
bind('n', '<Leader>p', '<cmd>lprev<CR>')

bind('n', '<C-j>', '<cmd>lua vim.diagnostic.goto_next()<CR>')
bind('n', '<C-k>', '<cmd>lua vim.diagnostic.goto_prev()<CR>')

bind('n', '<Leader>dd', '<cmd>lua vim.diagnostic.open_float()<CR>')
bind('n', '<Leader>dh', '<cmd>lua vim.diagnostic.hide()<CR>')
bind('n', '<Leader>ds', '<cmd>lua vim.diagnostic.show()<CR>')
bind('n', '<Leader>dl', '<cmd>lua vim.diagnostic.setloclist()<CR>')
bind('n', '<Leader>dq', '<cmd>lua vim.diagnostic.setqflist()<CR>')

local virtual_text_active = false
function _G.toggle_diagnostic_virtual_text()
    virtual_text_active = not virtual_text_active
    vim.diagnostic.config({ virtual_text = virtual_text_active })
end
bind('n', '<Leader>dv', '<cmd>lua toggle_diagnostic_virtual_text()<CR>')

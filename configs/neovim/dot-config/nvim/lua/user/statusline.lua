vim.opt.statusline = table.concat({
    '[%n] ',
    '%f ',
    '%q',
    '%m',
    '%r',
    '%h',
    '%w',
    '%=',
    '%=',
    '%y ',
    '%l/%L (%p%%) %c ',
})

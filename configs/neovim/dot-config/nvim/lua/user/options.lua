local set = vim.opt

-- Default <Tab> behavior
set.expandtab = true
set.tabstop = 8
set.softtabstop = 4
set.shiftwidth = 4
set.smarttab = true

set.autoindent = true
set.smartindent = true

set.inccommand = 'split'

set.mouse = 'nv'

set.splitbelow = true
set.splitright = true

set.hlsearch = true

set.number = true
set.relativenumber = true
set.wrap = false
set.colorcolumn = '+1'

set.formatoptions = 'cqjro/'
set.completeopt = { 'menu', 'menuone', 'noinsert' }

if vim.fn.executable('rg') then
    set.grepprg = "rg --vimgrep"
end

vim.diagnostic.config({
    severity_sort = true,
    virtual_text = false,
})

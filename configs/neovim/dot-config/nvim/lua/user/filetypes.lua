vim.filetype.add({
    pattern = {
        -- Podman Quadlet Systemd unit types
        ['.*/containers/systemd/.*%.container'] = 'systemd',
        ['.*/containers/systemd/.*%.kube'] = 'systemd',
        ['.*/containers/systemd/.*%.network'] = 'systemd',
        ['.*/containers/systemd/.*%.volume'] = 'systemd',
    },
})

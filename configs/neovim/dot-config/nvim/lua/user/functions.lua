local M = {}

function M.async_make(target, makeprg)
    local makeprg = makeprg or vim.api.nvim_get_option_value("makeprg", {})
    if not makeprg then return end

    local errorformat = vim.api.nvim_get_option_value("errorformat", {})
    if not errorformat then return end

    if target then
        makeprg = makeprg .. " " .. target.args
    end
    local cmd = vim.fn.expandcmd(makeprg)

    local lines = { "" }
    local function append_lines(job_id, data, event)
        if data then
            vim.list_extend(lines, data)
        end
    end

    local function populate_quickfix_list(job_id, data, event)
        vim.fn.setqflist({}, " ", {
            title = cmd,
            lines = lines,
            efm = errorformat,
        })
        vim.api.nvim_command("doautocmd QuickFixCmdPost")
        vim.api.nvim_command("cwindow")
        vim.api.nvim_echo({ { "Done: " .. cmd } }, false, {})
    end

    vim.api.nvim_echo({ { "Running: " .. cmd } }, false, {})
    vim.fn.jobstart(
        cmd,
        {
            on_stdout = append_lines,
            on_stderr = append_lines,
            on_exit = populate_quickfix_list,
            stdout_buffered = true,
            stderr_buffered = true,
        }
    )
end

local function index_of(array, item)
    for idx, value in ipairs(array) do
        if value == item then
            return idx
        end
    end
    return nil
end

function M.fzf_ui_select(items, opts, on_choice)
    local prompt = opts.prompt or "Select one of: "
    local format_item = opts.format_item or tostring

    local available_options = {}
    for i, item in ipairs(items) do
        table.insert(
            available_options,
            string.format("%d: %s", i, format_item(item))
        )
    end

    local function run_on_choice(lines)
        local _, chosen_line = next(lines)
        local chosen_idx = index_of(available_options, chosen_line)

        if chosen_idx then
            on_choice(items[chosen_idx], chosen_idx)
        else
            on_choice(nil, nil)
        end
    end

    local fzf_spec = vim.fn["fzf#wrap"]({
        source = available_options,
        sinklist = run_on_choice,
        options = {
            "--prompt", prompt,
            "--no-multi",
            "--reverse",
        },
        window = {
            width = 0.75,
            height = 0.25,
        },
    })
    vim.fn["fzf#run"](fzf_spec)
end

return M

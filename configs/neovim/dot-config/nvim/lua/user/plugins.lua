return require('packer').startup(function(use)
    -- Let Packer manage itself
    use 'wbthomason/packer.nvim'

    -- Tim Pope plugins
    use 'tpope/vim-commentary'
    use 'tpope/vim-dadbod'
    use 'tpope/vim-fugitive'
    use 'tpope/vim-repeat'
    use 'tpope/vim-surround'
    use 'tpope/vim-vinegar'

    -- Fuzzy finding
    use {
        'junegunn/fzf.vim',
        requires = { 'junegunn/fzf' },
        config = function()
            vim.g.fzf_layout = {
                window = { width = 0.9, height = 0.6 }
            }
        end,
    }
    use {
        'ojroques/nvim-lspfuzzy',
        requires = {
            { 'junegunn/fzf' },
            { 'junegunn/fzf.vim' }, -- required for preview
        },
        config = function()
            local function fzf_lines_to_locations(lines)
                local locations = {}
                for _, line in ipairs(lines) do
                    local next_field = string.gmatch(line, '([^:]+)')
                    table.insert(locations, {
                        filename = next_field(),
                        lnum = next_field(),
                        col = next_field(),
                        text = next_field(),
                    })
                end
                return locations
            end
            require("lspfuzzy").setup {
                callback = function()
                    vim.cmd.normal('z.') -- center window vertically after jumping
                end,
                fzf_action = {
                    ['ctrl-t'] = 'tab split',
                    ['ctrl-x'] = 'split',
                    ['ctrl-v'] = 'vsplit',
                    ['ctrl-q'] = function(lines)
                        vim.fn.setqflist(fzf_lines_to_locations(lines))
                        vim.fn.setqflist({}, 'a', { title = "Fuzzy LSP results" })
                        vim.cmd.copen()
                        vim.cmd.cc()
                    end,
                    ['ctrl-l'] = function(lines)
                        vim.fn.setloclist(0, fzf_lines_to_locations(lines))
                        vim.fn.setloclist(0, {}, 'a', { title = "Fuzzy LSP results" })
                        vim.cmd.lopen()
                        vim.cmd.ll()
                    end,
                },
            }
        end
    }

    -- Language support
    use 'dag/vim-fish'
    use 'rust-lang/rust.vim'

    -- Color themes
    use {
        'morhetz/gruvbox',
        config = function()
            if vim.fn.has('termguicolors') then
                vim.opt.termguicolors = true
            end
            vim.g.gruvbox_contrast_dark = "medium"
            vim.cmd.colorscheme({ "gruvbox" })
        end,
    }
end)

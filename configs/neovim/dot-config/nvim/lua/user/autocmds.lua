local user_group = vim.api.nvim_create_augroup("UserAutoCommands", { clear = true })

vim.api.nvim_create_autocmd("QuickFixCmdPost", {
    pattern = "grep",
    command = "cwindow", 
})

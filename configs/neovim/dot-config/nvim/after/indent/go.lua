-- Default GoIndent(v:lnum) expr is pretty buggy.
-- Prefer to rely on auto formatting on save using gopls.
vim.opt_local.indentexpr = nil

-- Start lua language server if installed
if vim.fn.executable('lua-language-server') then
    vim.lsp.start({
        name = 'lua-language-server',
        cmd = { 'lua-language-server' },
        root_dir = vim.fs.dirname(vim.fs.find({
            '.git',
        }, { upward = true })[1]),
        settings = {
            -- tailor lsp settings for Neovim config
            Lua = {
                runtime = {
                    version = "LuaJIT",
                },
                workspace = {
                    checkThirdParty = false,
                    library = {
                        vim.env.VIMRUNTIME,
                    }
                },
            }
        },
    })
end

-- Start LSP server
if vim.fn.executable('gopls') then
vim.lsp.start({
    name = 'gopls',
    cmd = {'gopls', '-remote=auto'},
    root_dir = vim.fs.dirname(vim.fs.find({
        'go.work',
        'go.mod',
        '.git',
    }, { upward = true })[1]),
    init_options = {
        analyses = {
            unusedparams = true,
        },
        staticcheck = true,
    }
})
end

-- Options
vim.opt_local.expandtab = false
vim.opt_local.tabstop = 4

vim.cmd('compiler go')
-- let make take precedence if a Makefile exists 
if vim.fn.filereadable('Makefile') or vim.fn.filereadable('makefile') then
    if vim.fn.match(vim.fn.expand('%:t'), '\\w\\+_test\\.go') > -1 then
        vim.opt_local.makeprg = 'make test'
    else
        vim.opt_local.makeprg = 'make'
    end
end

-- Key bindings
local bind = function(mode, keys, action)
    vim.keymap.set(mode, keys, action, {
        buffer = 0,
        remap = false,
        silent = true,
    })
end

bind('n', '<Leader>mm', '<cmd>lua require("user.functions").async_make()<CR>')
bind('n', '<Leader>mb', '<cmd>lua require("user.functions").async_make(nil, "go build ./...")<CR>')
bind('n', '<Leader>mr', '<cmd>lua require("user.functions").async_make(nil, "go run %")<CR>')
bind('n', '<Leader>mt', '<cmd>lua require("user.functions").async_make(nil, "go test -fullpath ./...")<CR>')
bind('n', '<Leader>md', '<cmd>lua require("user.functions").async_make(nil, "go doc")<CR>')

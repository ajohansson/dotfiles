-- Start LSP server
if vim.fn.executable('rust-analyzer') then
vim.lsp.start({
    name = 'rust-analyzer',
    cmd = {'rust-analyzer'},
    root_dir = vim.fs.dirname(vim.fs.find({
        'Cargo.toml',
        '.git',
    }, { upward = true })[1]),
    init_options = {
        checkOnSave = {
            command = "clippy",
        },
        imports = {
            granularity = {
                group = "module",
            },
        },
    }
})
end

-- Key bindings
local bind = function(mode, keys, action)
    vim.keymap.set(mode, keys, action, {
        buffer = 0,
        remap = false,
        silent = true,
    })
end

bind('n', '<Leader>mm', '<cmd>Ccheck<CR>')
bind('n', '<Leader>mb', '<cmd>Cbuild<CR>')
bind('n', '<Leader>mr', '<cmd>Crun<CR>')
bind('n', '<Leader>mt', '<cmd>Ctest<CR>')
bind('n', '<Leader>md', '<cmd>Cdoc<CR>')
bind('n', '<Leader>mD', '<cmd>Cdoc --open<CR>')

-- Start python-lsp-server if installed
if vim.fn.executable('pylsp') then
    vim.lsp.start({
        name = 'python-lsp-server',
        cmd = { 'pylsp' },
        root_dir = vim.fs.dirname(vim.fs.find({
            'pyproject.toml',
            '.git',
        }, { upward = true })[1]),
        init_options = {
            pylsp = {
                plugins = {
                    rope_autoimport = {
                        enabled = true,
                        completions = {
                            enabled = false,
                        },
                    },
                },
            },
        },
    })
end

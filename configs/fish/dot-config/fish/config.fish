if status is-interactive
    # Make sure user preferences are set during terminal session.
    set --global --export EDITOR '/usr/bin/nvim'
    set --global --export PAGER '/usr/bin/less'

    # FZF settings
    set --global --export FZF_DEFAULT_OPTS '--border=rounded --layout=reverse --height=50% --min-height=15 --bind ctrl-a:toggle-all'
    set --global --export FZF_DEFAULT_COMMAND 'fd'
    set --global FZF_ALT_C_COMMAND "fd --type directory"
    set --global FZF_CTRL_T_COMMAND "fd --type file"
    set --global FZF_CTRL_T_OPTS "--preview 'bat --style=auto --color=always --line-range :250 {}'"

    # General abbreviations
    abbr --add --global mv mv -iv
    abbr --add --global cp cp -iv
    abbr --add --global mkdir mkdir -pv

    if type -q nvim
        abbr --add --global vim nvim
        abbr --add --global e nvim
    end
    if type -q kubectl
        abbr --add --global k kubectl
    end

    # Hooks
    if type -q zoxide
        zoxide init fish | source
    end
    if type -q direnv
        direnv hook fish | source
    end
end

# PATH extensions
if test -d "$HOME/.local/bin"
    fish_add_path --global --append "$HOME/.local/bin"
end
if test -d "$HOME/.cargo/bin"
    # Using pacman managed rustup, so cargo binaries are installed in /usr/bin/,
    # except when installing local projects (e.g. rustlings) with '--path .'.
    fish_add_path --global --append "$HOME/.cargo/bin"
end
if test -d "$HOME/.go"
    set --global --export GOPATH "$HOME/.go"
    set --global --export GOBIN "$GOPATH/bin"
    fish_add_path --global --append "$GOBIN"
end

# Make sure config paths are unified under $XDG_CONFIG_HOME/
if set -q XDG_CONFIG_HOME
    set --export --path KUBECONFIG "$XDG_CONFIG_HOME/kube/config"
end

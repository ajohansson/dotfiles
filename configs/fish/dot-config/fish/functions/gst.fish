function gst --wraps=nvim\ -c\ \'Gedit\ :\' --description alias\ gst\ nvim\ -c\ \'Gedit\ :\'
  nvim -c 'Gedit :' $argv; 
end

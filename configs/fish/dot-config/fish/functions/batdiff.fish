function batdiff --description "Git diff with syntax highligting via bat"
    git diff --name-only --relative --diff-filter=d | xargs bat --diff
end

function fish_user_key_bindings --description "User defined key bindings"

    # Use Vim key bindings and start in insert mode
    fish_vi_key_bindings insert

    # Emulate Vim cursor behavior
    set fish_cursor_default      block       blink
    set fish_cursor_insert       line        blink
    set fish_cursor_replace_one  underscore  blink
    set fish_cursor_visual       block       blink

    # Enable FZF key bindings
    fzf_key_bindings

    # Custom key bindings
    bind --silent --user --mode insert --sets-mode default jj "set fish_bind_mode default; commandline -f backward-char repaint-mode"
    bind --user --mode insert \cf forward-char

end

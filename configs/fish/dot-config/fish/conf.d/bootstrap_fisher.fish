# Bootstrap fisher plugin manager if not installed
set fisher_function "$XDG_CONFIG_HOME/fish/functions/fisher.fish"
if not test -e $fisher_function
    echo "Installing fisher plugin manager..."
    curl --location --output $fisher_function https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish
    echo "Fisher plugin manager installed. Remember to update plugins."
end

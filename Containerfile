FROM docker.io/archlinux/archlinux:base-devel

# Set system XDG directories
ENV XDG_DATA_DIRS=/usr/local/share:/usr/share \
    XDG_CONFIG_DIRS=/etc/xdg

# Add wheel group to sudoers
RUN echo '%wheel ALL=(ALL:ALL) NOPASSWD: ALL' > /etc/sudoers.d/wheel

# Make sure man pages are extracted (disabled in Arch container images)
RUN sed -i 's/usr\/share\/man\/\* //' /etc/pacman.conf

# Update packages and install dev tools
COPY configs/pacman/dot-config/pacman/devtools.txt /etc/devtools.txt
RUN pacman -S --refresh --sysupgrade --noconfirm - < /etc/devtools.txt

# Initialize database for manual pages
RUN mandb

# Add user account 'dev'
RUN useradd --create-home --user-group --groups wheel --shell /bin/bash dev
USER dev
ENV HOME=/home/dev

# Set user XDG directories
ENV XDG_CONFIG_HOME=$HOME/.config \
    XDG_CACHE_HOME=$HOME/.cache \
    XDG_DATA_HOME=$HOME/.local/share \
    XDG_STATE_HOME=$HOME/.local/state

# Install Rust toolchain and components
RUN rustup default stable
RUN --mount=type=bind,source=configs/rustup/dot-rustup/components.txt,target=$HOME/rust-components \
    <$HOME/rust-components xargs rustup component add 

# Install configuration files
WORKDIR $HOME/dotfiles
COPY --chown=dev:dev configs/ configs/
COPY --chown=dev:dev install stow.exclude .
# remove existing bash config files before installing dotfiles
RUN rm $HOME/.bashrc $HOME/.bash_profile \
    && ./install --all

# Install latest fish shell plugins
RUN fish -c "fisher update"

# Install Neovim plugin manager and update plugins
# TODO: skip downloading plugin manager after migrating to lazy.nvim (with bootstrapping)
RUN git clone --depth 1 https://github.com/wbthomason/packer.nvim \
    $XDG_DATA_HOME/nvim/site/pack/packer/start/packer.nvim
RUN nvim --headless -c 'PackerSync' -c 'quit'

WORKDIR $HOME/work

# Set fish shell as entrypoint
ENTRYPOINT ["/usr/bin/fish"]
